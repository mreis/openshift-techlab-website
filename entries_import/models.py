from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django.db import models
from django.contrib.auth.models import User


@python_2_unicode_compatible
class BenchmarkImport(models.Model):

    class Meta:
        verbose_name = _('benchmarks import')
        verbose_name_plural = _('benchmarks imports')

    def __str__(self):
        return "{} - {}".format(self.importer, self.date)

    importer = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        related_name='%(app_label)s_%(class)s'
    )
    date = models.DateTimeField(auto_now_add=True, blank=True)
    comment = models.TextField(blank=True, null=True)
