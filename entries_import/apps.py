from __future__ import unicode_literals

from django.apps import AppConfig


class EntriesImportConfig(AppConfig):
    name = 'entries_import'
