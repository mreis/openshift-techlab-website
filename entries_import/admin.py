from django.contrib import admin

from bulk_admin import BulkModelAdmin

from .models import BenchmarkImport

@admin.register(BenchmarkImport)
class BenchmarkImportAdmin(BulkModelAdmin):
    pass
