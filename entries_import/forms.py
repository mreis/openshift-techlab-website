#####
# Django includes
###
from django import forms

#####
# Third-Party includes
###
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Hidden

#####
# Project includes
###
from SSD.models import SSD
from software_configuration.models import OS, LinuxKernel
from hardware_configuration.models import CPU, CPUArchitecture
from manufacturer.models import Manufacturer

from django.contrib.auth.models import Group, User


class ImportBenchmarkBaseForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()

        self.helper.form_method = 'post'
        self.helper.form_enctype = 'multipart/form-data'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.add_input(Hidden('dryrun', True))

        super(ImportBenchmarkBaseForm, self).__init__(*args, **kwargs)

    file = forms.FileField()


class ImportSSDBenchmarkForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()

        self.helper.form_method = 'post'
        self.helper.form_action = '/ssd_import'
        self.helper.form_enctype = 'multipart/form-data'
        self.helper.form_id = 'import_ssd_benchmark_form'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.add_input(Hidden('dryrun', True))

        super(ImportSSDBenchmarkForm, self).__init__(*args, **kwargs)

    # Mandatory Fields
    ssd = forms.ModelChoiceField(SSD.objects.all())
    benchmark_type = forms.ChoiceField((
        ('l2', 'l2'),
        ('l3', 'l3'),
        ('stability', 'l2 stability'),
    ))

    # Optional Fields
    os = forms.ModelChoiceField(OS.objects.all(), required=False)
    kernel = forms.ModelChoiceField(LinuxKernel.objects.all(), required=False)
    cpu = forms.ModelChoiceField(CPU.objects.all(), required=False)

    file = forms.FileField()


class ImportHEPSPECForm(ImportBenchmarkBaseForm):
    def __init__(self, *args, **kwargs):
        super(ImportHEPSPECForm, self).__init__(*args, **kwargs)
        self.helper.form_action = '/hepspec_import'
        self.helper.form_id = 'import_hepspec_form'

    # Mandatory Fields
    file = forms.FileField()
    architecture = forms.ModelChoiceField(CPUArchitecture.objects.all())
    manufacturer = forms.ModelChoiceField(Manufacturer.objects.all())
#    owner = forms.CharField(strip=True)
#    vendor = forms.CharField(strip=True)

    # Optional Fields
    groups = forms.ModelMultipleChoiceField(Group.objects.all(), required=False)
    users = forms.ModelMultipleChoiceField(User.objects.all(), required=False)

