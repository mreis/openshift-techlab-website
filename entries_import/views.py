#####
# System includes
###
import re

#####
# Django includes
###
from django.shortcuts import render
from django.contrib.auth.models import User

#####
# Third-Party includes
###
from guardian.shortcuts import assign_perm

#####
# Project includes
###
from software_configuration.models import OS, SoftwareConfiguration
from hardware_configuration.models import HardwareConfiguration, CPU, Socket
from techlab_systems.models import SystemConfiguration
from benchmarks.models import (
    # HEPSpec06
    HEPSPEC06,
    # ssdbench_l2
    SSDBenchL2, SSDBenchL2Stability,
    # ssdbench_l3
    SSDBenchL3Analytics, SSDBenchL3Checkpointing, SSDBenchL3HFT,
    SSDBenchL3Db8kpage, SSDBenchL3BigBlock, SSDBenchL3OLTP, SSDBenchL3Metadata,
)
from benchmarks.utils import dec_or_none
from entries_import.forms import ImportSSDBenchmarkForm, ImportHEPSPECForm
from entries_import.models import BenchmarkImport

def import_l2_csv(data, request):
    # Get Form data
    ssd = data['ssd']
    filename = data['file']

    os = data['os']
    kernel = data['kernel']
    cpu = data['cpu']

    columns = 'percent_capacity_used,acces_type,percent_write,block_size,threads,qd_per_thread,iops,throughput,read_latency,write_latency,comment'
    content = filename.read().decode('utf-8')
    filename.close()
    pattern = re.compile("^[^,\n]+$", re.VERBOSE | re.MULTILINE)
    block_indices = [match.start() for match in pattern.finditer(content)]
    block_indices.append(len(content))
    added, ignored = [], []

    # Create the import object
    user = User.objects.get(username=request.user)
    entries_import = BenchmarkImport(importer=user)
    entries_import.save()

    for i in range(len(block_indices) - 1):
        block = content[block_indices[i]:block_indices[i + 1]].strip('\n').split('\n')
        test_description = block[0]
        runs = block[1:]
        for run in runs:
            args = run.split(',')
            try:
                comment = args[10]
            except IndexError:
                comment = ''
            kwargs = dict(
                test_description=test_description,
                percent_capacity_used=int(args[0]),
                access_type=SSDBenchL2.import_access_type(args[1]),
                percent_write=int(args[2]),
                block_size=int(args[3]),
                threads=int(args[4]),
                qd_per_thread=int(args[5]),
                iops=dec_or_none(args[6]),
                throughput=int(args[7]),
                read_latency=dec_or_none(args[8]),
                write_latency=dec_or_none(args[9]),
                comment=comment,
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
            )

#            if data['dryrun']:
#                created = False
#                try:
#                    obj = SSDBenchL2.objects.get(kwargs)
#                except MultipleObjectsReturned:
#                    pass
#                except DoesNotExist:
#                    created = True
#            else:
#                obj, created = SSDBenchL2.objects.get_or_create(kwargs)
            obj, created = SSDBenchL2.objects.get_or_create(**kwargs)
            if created:
                added.append(run)
            else:
                ignored.append(run)
    return added, ignored


def import_l3_csv(data, request):
    # Get Form data
    ssd = data['ssd']
    os = data['os']
    kernel = data['kernel']
    cpu = data['cpu']

    # Read uploaded file content
    content = data['file'].read().decode('utf-8')
    data['file'].close()

    added, ignored = [], []

    # Create the import object
    user = User.objects.get(username=request.user)
    entries_import = BenchmarkImport(importer=user)
    entries_import.save()

    for line in content.splitlines()[1:]:
        values = line.split(',')
        obj, created = None, False
        if values[0] == 'analytics':
            obj, created = SSDBenchL3Analytics.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                rdmbps=values[9], wrmbps=values[5], rdiops=values[11], wriops=values[7]
            )
        elif values[0] == 'checkpointing':
            obj, created = SSDBenchL3Checkpointing.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                rdmbps=values[9], wrmbps=values[5], rdiops=values[11], wriops=values[7]
            )
        elif values[0] == 'hft':
            obj, created = SSDBenchL3HFT.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                rdmbps=values[9], wrmbps=values[5], rdiops=values[11], wriops=values[7]
            )
        elif values[0] == 'db8kpage':
            obj, created = SSDBenchL3Db8kpage.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                rdmbps=values[9], wrmbps=values[5], rdiops=values[11], wriops=values[7]
            )
        elif values[0] == 'bigblock':
            obj, created = SSDBenchL3BigBlock.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                rdmbps=values[9], wrmbps=values[5], rdiops=values[11], wriops=values[7]
            )
        elif values[0] == 'oltp':
            obj, created = SSDBenchL3OLTP.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                p99rd=values[5], p99wr=values[7]
            )
        elif values[0] == 'metadata':
            obj, created = SSDBenchL3Metadata.objects.get_or_create(
                ssd=ssd, os=os, cpu=cpu, kernel=kernel,
                benchmark_import=entries_import,
                creation=values[5], deletion=values[7]
            )
        if created:
            added.append("ssdbench_l3 {} for {}: {}".format(values[0], ssd, line))
        else:
            ignored.append(line)

    return added, ignored


def import_l2_stability_csv(data, request):
    # Get Form data
    ssd = data['ssd']
    os = data['os']
    kernel = data['kernel']
    cpu = data['cpu']

    # Read uploaded file content
    content = data['file'].read().decode('utf-8')
    data['file'].close()

    iops_array = [value for value in content.splitlines()[1:]]
    obj, created = SSDBenchL2Stability.objects.get_or_create(
        ssd=ssd, os=os, cpu=cpu, kernel=kernel, iops=iops_array
    )
    added, ignored = [], []
    entry = "stability test for ssd {}: {}".format(ssd, iops_array)
    if created:
        added.append(entry)
    else:
        ignored.append(entry)
    return added, ignored


def import_benchmarks(request):
    filename = "No file uploaded"
    content = ""
    if request.method == 'POST':
        form = ImportSSDBenchmarkForm(request.POST, request.FILES)
        if form.is_valid():
            added, ignored = [], []
            if(request.POST['benchmark_type'] == 'l2'):
                added, ignored = import_l2_csv(form.cleaned_data, request)
            elif(request.POST['benchmark_type'] == 'stability'):
                added, ignored = import_l2_stability_csv(form.cleaned_data, request)
            elif(request.POST['benchmark_type'] == 'l3'):
                added, ignored = import_l3_csv(form.cleaned_data, request)
            if added != []:
                content += "The following entries have been added\n"
                for line in added:
                    content += "{}\n".format(line)
            if ignored != []:
                content += "The following entries have been ignored\n"
                for line in ignored:
                    content += "{}\n".format(line)

#        if form.is_valid():
#            print("######### DEBUG / form is valid")
#            content = form.cleaned_data['file'].read()
#            if request.is_ajax():
#                print("############ DEBUG / AJAX")
#                if form.data['dryrun']:
#                    content = "Dry run, data will be not created"
#                else:
#                    content = "You have confirmed, data will be created"
#                return HttpResponse(content)
#            else:
#                print("############ DEBUG / NOT AJAX")
#                content = "Form is OK (no AJAX)"
#                pass
#        else:
#            print("######### DEBUG / form is NOT valid")
#            if request.is_ajax()
#                print("############ DEBUG / AJAX")
#                errors_dict = {}
#                if form.errors:
#                    for error in form.errors:
#                        errors_dict[error] = str(form.errors[error])
#                return HttpResponseBadRequest(json.dumps(errors_dict))
#            else:
#                print("############ DEBUG / NOT AJAX")
#                content = "Form has errors (no AJAX)"
#                pass

    else:
        print("###### DEBUG / request.method is *NOT* POST")
        form = ImportSSDBenchmarkForm()
    return render(request, 'benchmarks/import_benchmarks.html', {'form': form, 'content': content})


def import_hepspec_benchmarks(request):
    if not request.user.is_authenticated():
        context = {'message': 'You need to login in order to import benchmarks.'}
        template = 'benchmarks/please_authenticate.html'
    else:
        filename = "No file uploaded"
        message = ""
        if request.method == 'POST':
            form = ImportHEPSPECForm(request.POST, request.FILES)
            if form.is_valid():
                data = form.cleaned_data
                added, ignored = [], []
                filename = data['file']
                content = [line.rstrip().decode('utf-8') for line in filename.readlines()]
                filename.close()
                header = content[0].split(',')
                raw_entries = [line.split(',') for line in content[1:]]
                entries = []
                for line in raw_entries:
                    for i in range(1, len(header)):
                        if line[i]:
                            entries.append([line[0], header[i], line[i]])

                user = User.objects.get(username=request.user)
                entries_import = BenchmarkImport(importer=user)
                entries_import.save()

                os, created = OS.objects.get_or_create(name="Unspecified", version="NA")
                software_configuration, created = SoftwareConfiguration.objects.get_or_create(name="Unspecified", os=os)
#                owner = data['owner']
                groups = data['groups']
                users = data['users']
                architecture = data['architecture']
                manufacturer = data['manufacturer']

                for entry in entries:
                    print("DEBUG: cpu {} - {} - {}".format(entry[0], architecture, manufacturer))
                    cpu, created = CPU.objects.get_or_create(
                        model_name=entry[0],
                        architecture=architecture,
                        manufacturer=manufacturer,
                    )
                    if created:
                        print("DEBUG: Added")
                    hardware_configuration, created = HardwareConfiguration.objects.get_or_create(
                        name=entry[0],
                    )
                    print("DEBUG: hw conf {}".format(entry[0]))
                    if created:
                        print("DEBUG: hw conf created, adding socket with {}".format(entry[0]))
                        socket = Socket(
                            cpu=cpu,
                            hardware_configuration=hardware_configuration
                        )
                        socket.save()
                    if created:
                        for user in users:
                            print("USER: {}".format(user))
                            assign_perm('view_bench', user, hardware_configuration)
                            print("User {} has perm on {}? {}".format(user, hardware_configuration, user.has_perm('view_bench', hardware_configuration)))
                            user.save()
                        for group in groups:
                            assign_perm('view_bench', group, hardware_configuration)
                            group.save()
                    system_configuration, created = SystemConfiguration.objects.get_or_create(
                        hardware_configuration=hardware_configuration,
                        software_configuration=software_configuration,
                    )
                    obj, created = HEPSPEC06.objects.get_or_create(
                        system_configuration=system_configuration,
                        nb_workers=entry[1],
                        score=entry[2],
                        benchmark_import=entries_import,
                    )
                    if created:
                        added.append(entry)
                    else:
                        ignored.append(entry)
                if added != []:
                    message += "The following entries have been added\n"
                    for line in added:
                        message += "{}\n".format(line)
                if ignored != []:
                    message += "The following entries have been ignored\n"
                    for line in ignored:
                        message += "{}\n".format(line)

                context = {'form': form, 'content': message}
                template = 'benchmarks/import_cpu_benchmarks.html'
            else:
                context = {'content': 'Invalid form'}
                template = 'benchmarks/please_authenticate.html'
        else:
            form = ImportHEPSPECForm()
            context = {'form': form, 'content': 'Please upload a file'}
            template = 'benchmarks/import_cpu_benchmarks.html'

    return render(request, template, context)
