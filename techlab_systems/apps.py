from __future__ import unicode_literals

from django.apps import AppConfig


class TechlabSystemsConfig(AppConfig):
    name = 'techlab_systems'
    verbose_name = 'Techlab Systems'
