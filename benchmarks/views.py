# -*- coding: utf-8 -*-
#####
# System includes
###
import os
import json
import pandas
import numpy
import seaborn
from math import floor, log
from collections import OrderedDict

#####
# Django includes
###
from django.shortcuts import render, redirect

#####
# Third-party includes
###

# Bokeh
from bokeh import palettes
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.layouts import column, row
from bokeh.models import (
    CustomJS,
    ColumnDataSource,
    Legend,
    HoverTool,
    PanTool,
    BoxZoomTool,
    CrosshairTool,
    ResetTool,
    SaveTool,
    WheelZoomTool,
)
from bokeh.models.widgets import (
    Slider,
    Select,
    CheckboxGroup,
    Div,
    RadioGroup,
    CheckboxButtonGroup,
    RadioButtonGroup,
    DateRangeSlider,
    DatePicker
)

#####
# Project includes
###
from SSD.models import SSD
from software_configuration.models import OS, LinuxKernel, SoftwareConfiguration
from hardware_configuration.models import CPU, HardwareConfiguration
from benchmarks.filters import build_filters
from benchmarks.fields import init_fields
from benchmarks.plots import init_plots
from benchmarks.utils import *
from benchmarks.models import (
    # HEPSpec06
    HEPSPEC06,
    # ssdbench_l2
    SSDBenchL2,
    SSDBenchL2Stability,
    # ssdbench_l3
    SSDBenchL3Analytics,
    SSDBenchL3Checkpointing,
    SSDBenchL3HFT,
    SSDBenchL3Db8kpage,
    SSDBenchL3BigBlock,
    SSDBenchL3OLTP,
    SSDBenchL3Metadata,
)
from benchmarks.forms import SelectCPUs, SelectSSDs


#####
# Views
###
def benchmarks_list(request):
    return render(request, "benchmarks/benchmarks_list.html", {})


def cpu_benchmarks(request):

    # Get data
    form = SelectCPUs(request.POST, user=request.user)
    runs = get_runs_for_user(request, form)
    if not runs:
        return render(request, "benchmarks/no_privilege.html", {})

    # Select fields
    fields = init_fields('benchmarks/fields.json', ['common', 'cpu_common', 'hepspec06'])

    # Build bokeh data sources
    configuration_pks = list(set([run.system_configuration.pk for run in runs]))

    # Build raw DataFrame with all necessary fields from the HEPSPEC06 runs
    df_data = {}
    for field in fields:
        print("DEBUG: {}".format(field))
        print("df_data['{name}'] = [{dtype}(run.{path}) for run in runs]".format(name=field['name'], dtype=field['dtype'], path=field['path']))
        exec("df_data['{name}'] = [{dtype}(run.{path}) for run in runs]".format(name=field['name'], dtype=field['dtype'], path=field['path']))
    df = pandas.DataFrame(df_data)

    grouped_df = df.groupby(['pk', 'nb_workers']).agg({field['name']: field['agg'] for field in fields})
    ds = ColumnDataSource()
    ds.add(configuration_pks, 'pks')

    for pk in configuration_pks:
        for field in fields:
            ds.add(list(grouped_df[grouped_df['pk'] == pk][field['name']].as_matrix()), '{}_{}'.format(field['name'], pk))

    original_ds = ColumnDataSource()
    for key in ds.column_names:
        original_ds.add(ds.data[key], key)

    # Build plots
    plots = [
        {'title': 'Score / Workers', 'x': 'nb_workers', 'y': 'score', 'source': ds},
        {'title': 'Active Energy / Workers', 'x': 'nb_workers', 'y': 'active_energy', 'source': ds},
        {'title': 'Apparent Energy / Workers', 'x': 'nb_workers', 'y': 'apparent_energy', 'source': ds},
        {'title': 'Score / Active Energy', 'x': 'score', 'y': 'active_energy', 'source': ds},
    ]
    for plot in plots:
        tools = [
            PanTool(), BoxZoomTool(), CrosshairTool(),
            WheelZoomTool(), ResetTool(), SaveTool()
        ]
        plot['figure'] = figure(title=plot['title'], toolbar_location='right', tools=tools, x_axis_label=plot['x'], y_axis_label=plot['y'])
        pks = plot['source'].data['pks']
        legends = []
        for n, pk, color in zip(range(10), pks, palettes.magma(len(pks))):
            # exec("legend_text = plot['source'].data['system_configuration_{}'][0]"    .format(pk))
            legend_text = runs.filter(system_configuration__pk=pk)[0].system_configuration.hardware_configuration.name
            line = plot['figure'].line(
                "{}_{}".format(plot['x'], pk),
                "{}_{}".format(plot['y'], pk),
                source=plot['source'],
                line_width=3, line_alpha=0.5, color=color
            )
            circle = plot['figure'].circle(
                "{}_{}".format(plot['x'], pk),
                "{}_{}".format(plot['y'], pk),
                source=plot['source'],
                size=8, fill_color='white', color=color
            )
            legends += [(legend_text, [line, circle])]
        legend = Legend(legends=legends, location=(60, 0))
        plot['figure'].add_layout(legend, 'below')

    # Build filters
    filters = OrderedDict()
    filters['nb_workers_min'] = {'type': 'slider_min', 'category': 'benchmark', 'description': 'Filter benchmark runs below a minimum number of workers.', 'ds_column': 'nb_workers', 'init': min}
    filters['nb_workers_max'] = {'type': 'slider_max', 'category': 'benchmark', 'description': 'Filter benchmark runs above a maximum number of workers.', 'ds_column': 'nb_workers', 'init': max}
    filters['architecture'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Select hardware architectures.', 'ds_column': 'architecture'}
    filters['os'] = {'type': 'checkbox_group', 'category': 'software', 'description': 'Select operating systems.', 'ds_column': 'os'}
    filters['system'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Select a configuration.', 'ds_column': 'system_configuration'}

    pks = [pk for pk in original_ds.data['pks']]
    for name, f in filters.items():
        value_set = sorted(list(set(
            [value for value in sum([original_ds.data['{}_{}'.format(f['ds_column'], pk)] for pk in pks], [])]
        )))
        if f['type'].startswith('slider'):
            step = 1 if is_list_of_integer(value_set) else 10**floor(log(min([value_set[i + 1] - value_set[i] for i in range(len(value_set)) if i + 1 < len(value_set)]), 10))
            f['object'] = Slider(
                title=name,
                start=min(value_set),
                end=max(value_set),
                value=f['init'](value_set),
                step=step
            )
        elif f['type'] == 'checkbox_group':
            f['object'] = CheckboxGroup(labels=value_set)
        elif f['type'] == 'checkbox_button_group':
            f['object'] = CheckboxButtonGroup(labels=value_set)
        elif f['type'] == 'select':
            f['object'] = Select(
                title=name, value=value_set[0], options=value_set
            )

    callback_code = """
var data = source.data;
var original_data = original_source.data;

// Init filter values
{init_filter_values}

for ( var j = 0; j < original_data['pks'].length; j++ ) {{
    var pk = original_data['pks'][j];
    data['nb_workers_' + pk] = [];
    data['score_' + pk] = [];
    for ( var i=0; i<original_data[Object.keys(original_data)[0]].length; i++ ) {{
        if (
            {filter_condition}
        ) {{
            data['nb_workers_' + pk].push(original_data['nb_workers_' + pk][i]);
            data['score_' + pk].push(original_data['score_' + pk][i]);
        }}
    }}
}}

source.trigger('change');
""".format(
        init_filter_values=init_filter_values(filters),
        filter_condition=init_filter_condition(filters, "pk")
    )

    # now define the callback objects now that the filter widgets exist
    callback_source_args = {'source': ds, 'original_source': original_ds}
    callback_filter_args = {k: v['object'] for k, v in filters.items()}
    callback_args = dict(callback_filter_args, **callback_source_args)

    generic_callback = CustomJS(
        args=callback_args,
        code=callback_code
    )

    for name, f in filters.items():
        f['object'].callback = generic_callback

    # Build component
    plots_layout = column([plot['figure'] for plot in plots], sizing_mode='scale_width')
#    plots2 = row(plot2, sizing_mode='scale_width')
    widgets_list = [
        Div(text="""<p>
            Below are various filters that will update in real-time the benchmarks displayed on this page.<br>
            Each filter further reduces either the set of CPUs considered or the set of benchmarks displayed for each of the selected CPU models.<br>
            At any moment you may hide/show this filters tab by clicking the 'Show/Hide filters' button.
            </p>"""
        )
    ]
    for name, f in filters.items():
        widgets_list.append(Div(text="<h3>{}</h3><p>{}</p>".format(name, f['description'])))
        widgets_list.append(f['object'])
    widgets = column([w for w in widgets_list], sizing_mode='scale_width')
#    widgets = column([f['object'] for name, f in filters.items()], sizing_mode='scale_width')

    script, elements_to_render = components([widgets, plots_layout], wrap_plot_info=False)
    context = {
        'bokeh_script': script,
        'bokeh_widgets': [elements_to_render[0]],
        'bokeh_plots': elements_to_render[1:],
        'form': form,
    }

    return render(request, 'benchmarks/bokeh_base_dev.html', context)
    # return render(request, 'benchmarks/bokeh_base.html', context)


def ssd_benchmarks(request):
    # Check POST data. If absent, no ssd preselection has been made and
    # the request must be redirected to the selection page.
    if not request.POST:
        return redirect('ssd_benchmarks_select')

    # Get data
    form = SelectSSDs(request.POST, user=request.user)
    l2_entries = get_l2_entries_for_user(request, form)
    if not l2_entries:
        return render(request, "benchmarks/no_privilege.html", {})

    fields = init_fields('benchmarks/fields.json', ['common', 'ssd_common', 'ssdbenchl2'])

    # Build bokeh data sources
    ssd_pks = list(set([entry.ssd.pk for entry in l2_entries]))

    # Build raw DataFrame with all necessary fields
    df_data = {}
    for field in fields:
        exec("df_data['{name}'] = [{dtype}(entry.{path}) for entry in l2_entries]".format(name=field['name'], dtype=field['dtype'], path=field['path']))
    df = pandas.DataFrame(df_data)

    grouped_df = df.groupby(['pk', 'test_description']).agg({field['name']: field['agg'] for field in fields})
    original_ds = ColumnDataSource()
    original_ds.add(ssd_pks, 'pks')
    ds = ColumnDataSource()
    ds.add(ssd_pks, 'pks')

    for pk in ssd_pks:
        for field in fields:
            original_ds.add(list(grouped_df[grouped_df['pk'] == pk][field['name']].as_matrix()), '{}_{}'.format(field['name'], pk))
            ds.add(list(grouped_df[grouped_df['pk'] == pk][field['name']].as_matrix()), '{}_{}'.format(field['name'], pk))

    # Build plots
    tests = init_plots('benchmarks/plots.json', ['ssdbenchl2'])

    for test in tests:
        test['test_id'] = str(next(gen_id))
        test['dataframe'] = df[df['test_description'] == test['description']].groupby(['pk', 'test_description', test['x']]).agg({field['name']: field['agg'] for field in fields})
        test['datasource'] = ColumnDataSource()
        test['datasource'].add(ssd_pks, 'pks')
        test['original_datasource'] = ColumnDataSource()
        test['original_datasource'].add(ssd_pks, 'pks')
        test['x_axis_label'] = u'{} ({})'.format(test['x'], test['x_unit']) if 'x_unit' in test.keys() else test['x']
        test['y_axis_label'] = u'{} ({})'.format(test['y'], test['y_unit']) if 'y_unit' in test.keys() else test['y']
        for pk in ssd_pks:
            for field in fields:
                test['datasource'].add(list(test['dataframe'][test['dataframe']['pk'] == pk][field['name']].as_matrix()), '{}_{}'.format(field['name'], pk))
                test['original_datasource'].add(list(test['dataframe'][test['dataframe']['pk'] == pk][field['name']].as_matrix()), '{}_{}'.format(field['name'], pk))

    for test in tests:
        tools = [PanTool(), BoxZoomTool(), CrosshairTool(), WheelZoomTool(), ResetTool(), SaveTool()]
        test['figure'] = figure(title="{} ({} / {})".format(test['description'], test['y'], test['x']), toolbar_location='right', tools=tools, x_axis_label=test['x_axis_label'], y_axis_label=test['y_axis_label'])
        pks = test['datasource'].data['pks']
        legends = []
#        for n, pk, color in zip(range(10), pks, palettes.magma(len(pks))):
#        for n, pk, color in zip(range(10), pks, palettes.Spectral[len(pks)]):
        for n, pk, color in zip(range(15), pks, seaborn.color_palette("deep", len(pks)).as_hex()):
            legend_text = l2_entries.filter(ssd__pk=pk)[0].ssd.__str__()
            line = test['figure'].line("{}_{}".format(test['x'], pk), "{}_{}".format(test['y'], pk), source=test['datasource'], line_width=3, line_alpha=0.5, color=color)
            circle = test['figure'].circle("{}_{}".format(test['x'], pk), "{}_{}".format(test['y'], pk), source=test['datasource'], size=8, fill_color='white', color=color)
            legends += [(legend_text, [line, circle])]
        legend = Legend(legends=legends, location=(60, 0))
        test['figure'].add_layout(legend, 'below')

#    # Stability runs
#    l2_stability_entries = SSDBenchL2Stability.objects.filter(ssd__in=allowed_ssds)
#    l2_stability_ssd_pks = list(set([entry.ssd.pk for entry in l2_stability_entries]))
#
#    l2_stability_fields = init_fields('benchmarks/fields.json', ['common', 'ssd_common'])
#
#    # Build raw DataFrame with all necessary fields
#    l2_stability_df_data = {}
#    for entry in l2_stability_entries:
#        for field in l2_stability_fields:
#            exec("l2_stability_df_data['{name}_{pk}'] = [{dtype}(entry.{path})] * len(entry.iops)".format(name=field['name'], dtype=field['dtype'], path=field['path'], pk=entry.pk))
#        exec("l2_stability_df_data['iops_{pk}'] = entry.iops".format(pk=entry.pk))
#        exec("l2_stability_df_data['s_{pk}'] = range(len(entry.iops))".format(pk=entry.pk))
#    l2_stability_df = pandas.DataFrame(dict([(k, pandas.Series(v)) for k, v in l2_stability_df_data.iteritems()]))
#
#    l2_stability_original_ds = ColumnDataSource(l2_stability_df)
#    l2_stability_ds = ColumnDataSource(l2_stability_df)
#
#    # Build plots
#    l2_stability_tests = [
#        {"description": "IOPS stability: IOPS/s", "x": "s", "y": "iops", "test_id": "10"},
#    ]
#
#    l2_stability_plots = []
#    tools = [PanTool(), BoxZoomTool(), CrosshairTool(), WheelZoomTool(), ResetTool(), SaveTool()]
#    plot = figure(title="IOPS stability (iops/s)", toolbar_location='right', tools=tools, x_axis_label="time (s)", y_axis_label="iops")
#    legends = []
#    for n, entry, color in zip(range(15), l2_stability_entries, seaborn.color_palette("deep", len(l2_stability_entries)).as_hex()):
#        legend_text = entry.ssd.__str__()
#        circle = plot.circle("s_{}".format(entry.pk), "iops_{}".format(entry.pk), source=l2_stability_ds, size=2, fill_color='white', color=color)
#        legends += [(legend_text, [circle])]
#    legend = Legend(legends=legends, location=(60, 0))
#    plot.add_layout(legend, 'below')
#    l2_stability_plots.append(plot)

    # Build filters
    filters = OrderedDict()
    filters['ssd'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Filter ssds by model', 'ds_column': 'ssd'}
    filters['os'] = {'type': 'checkbox_group', 'category': 'software', 'description': 'Filter oss by model', 'ds_column': 'os'}
    filters['manufacturer'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Filter ssds by manufacturer', 'ds_column': 'manufacturer'}
    filters['form_factor'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Filter ssds by form_factor', 'ds_column': 'form_factor'}
    filters['interface'] = {'type': 'checkbox_group', 'category': 'hardware', 'description': 'Filter ssds by interface', 'ds_column': 'interface'}
    filters['capacity_min'] = {'type': 'slider_min', 'category': 'hardware', 'description': 'Filter ssds below a minimum capacity', 'ds_column': 'capacity', 'init': min}
    filters['capacity_max'] = {'type': 'slider_max', 'category': 'hardware', 'description': 'Filter ssds above a maximum capacity', 'ds_column': 'capacity', 'init': max}

    for name, f in filters.items():
        value_set = sorted(list(set([value for value in sum([original_ds.data['{}_{}'.format(f['ds_column'], pk)] for pk in ssd_pks], [])])))
        if f['type'].startswith('slider'):
            step = 1 if is_list_of_integer(value_set) else 10**floor(log(min([value_set[i + 1] - value_set[i] for i in range(len(value_set)) if i + 1 < len(value_set)]), 10))
            f['object'] = Slider(
                title=name,
                start=min(value_set),
                end=max(value_set),
                value=f['init'](value_set),
                step=step
            )
        elif f['type'] == 'checkbox_group':
            f['object'] = CheckboxGroup(labels=value_set)
        elif f['type'] == 'checkbox_button_group':
            f['object'] = CheckboxButtonGroup(labels=value_set)
        elif f['type'] == 'select':
            f['object'] = Select(
                title=name,
                value=value_set[0],
                options=value_set
            )

    callback_base = """
var data_{test_id} = source_{test_id}.data;
var original_data_{test_id} = original_source_{test_id}.data;

for ( var j = 0; j < original_data_{test_id}['pks'].length; j++ ) {{
    var pk = original_data_{test_id}['pks'][j];
    data_{test_id}['{x}_' + pk] = [];
    data_{test_id}['{y}_' + pk] = [];
    for ( var i=0; i<original_data_{test_id}[Object.keys(original_data_{test_id})[0]].length; i++ ) {{
        if (
            {filter_condition}
        ) {{
            data_{test_id}['{x}_' + pk].push(original_data_{test_id}['{x}_' + pk][i]);
            data_{test_id}['{y}_' + pk].push(original_data_{test_id}['{y}_' + pk][i]);
        }}
    }}
}}

source_{test_id}.trigger('change');
"""
    callback_code_chunks = []
    callback_code_chunks.append(init_filter_values(filters))
    for test in tests:
        callback_code_chunks.append(callback_base.format(
            filter_condition=init_filter_condition(filters, "pk", test['test_id']),
            test_id=test['test_id'], x=test['x'], y=test['y']
        ))
    callback_code = "\n".join(callback_code_chunks)

    # now define the callback objects now that the filter widgets exist
    callback_source_args = {}
    for test in tests:
        callback_source_args['original_source_{test_id}'.format(test_id=test['test_id'])] = test['original_datasource']
        callback_source_args['source_{test_id}'.format(test_id=test['test_id'])] = test['datasource']
    callback_filter_args = {k: v['object'] for k, v in filters.items()}
    callback_args = dict(callback_filter_args, **callback_source_args)

    generic_callback = CustomJS(
        args=callback_args,
        code=callback_code
    )

    for name, f in filters.items():
        f['object'].callback = generic_callback

    # Build component
    plots_layout = column(
        [plot['figure'] for plot in tests],
        sizing_mode='scale_width',
    )
    plots_layout = [column(plot['figure'], sizing_mode='scale_width') for plot in tests]  # + [column(plot, sizing_mode='scale_width') for plot in l2_stability_plots]
    widgets_list = [
        Div(text="""<p>
            Below are various filters that will update in real-time the benchmarks displayed on this page.<br>
            Each filter further reduces either the set of SSDs considered or the set of benchmarks displayed for each of the selected SSD models.<br>
            At any moment you may hide/show this filters tab by clicking the 'Show/Hide filters' button.
            </p>"""
        )
    ]
    for name, f in filters.items():
        widgets_list.append(Div(text="<h3>{}</h3><p>{}</p>".format(name, f['description'])))
        widgets_list.append(f['object'])
    widgets = column([w for w in widgets_list], sizing_mode='scale_width')

    script, elements_to_render = components(
        [element for element in [widgets] + plots_layout],
        wrap_plot_info=False
    )
    context = {
        'bokeh_script': script,
        'bokeh_widgets': [elements_to_render[0]],
        'bokeh_plots': elements_to_render[1:],
    }

    return render(request, 'benchmarks/bokeh_base_dev.html', context)


def select_hepspec_source(request):
  form = SelectCPUs(user=request.user)

  context = {
      'form': form
  }

  return render(request, 'benchmarks/select_source.html', context)

def select_ssd_benchmark_source(request):
    form = SelectSSDs(user=request.user)

    context = {
        'form': form
    }

    return render(request, 'benchmarks/select_source.html', context)
