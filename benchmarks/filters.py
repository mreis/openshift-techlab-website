import os
import json
from django.conf import settings
from collections import OrderedDict
from bokeh.models.widgets import (
    Slider, Select, CheckboxGroup, Div, RadioGroup, CheckboxButtonGroup,
    RadioButtonGroup, DateRangeSlider, DatePicker
)
from benchmarks.utils import *

def build_filters(json_file, benchmark_type, ds):
    with open(os.path.join(settings.BASE_DIR, json_file), 'r') as f:
      all_filters = json.load(f, object_pairs_hook=OrderedDict)
    filters = all_filters[benchmark_type]

    for name, f in filters.items():
        value_set = sorted(list(set([value for value in sum([ds.data['{}_{}'.format(f['ds_column'], pk)] for pk in ds.data['pks']], [])])))
        if f['type'].startswith('slider'):
            step = 1 if is_list_of_integer(value_set) else 10**floor(log(min([value_set[i + 1] - value_set[i] for i in range(len(value_set)) if i + 1 < len(value_set)]), 10))
            f['object'] = Slider(
                start=min(value_set),
                end=max(value_set),
                value=eval(f['init'])(value_set),
                step=step
            )
        elif f['type'] == 'range_slider':
            step = 1 if is_list_of_integer(value_set) else 10**floor(log(min([value_set[i + 1] - value_set[i] for i in range(len(value_set)) if i + 1 < len(value_set)]), 10))
            start=min(value_set)
            end=max(value_set)
            f['object'] = RangeSlider(
                start=start,
                end=end,
                range=(start,end),
                step=step
            )
        elif f['type'] == 'checkbox_group':
            f['object'] = CheckboxGroup(labels=value_set)
        elif f['type'] == 'checkbox_button_group':
            f['object'] = CheckboxButtonGroup(labels=value_set)
        elif f['type'] == 'select':
            f['object'] = Select(
                title=name,
                value=value_set[0],
                options=value_set
            )
        elif f['type'] == 'date_range_slider':
            earliest_date = value_set[0]
            latest_date = value_set[len(value_set)-1]
            f['object'] = DateRangeSlider(
                bounds=(earliest_date, latest_date),
                value=(earliest_date, latest_date),
            )
        elif f['type'].startswith('date_picker'):
            f['object'] = DatePicker(
                min_date=min(value_set),
                max_date=max(value_set),
                value=eval(f['init'])(value_set),
            )

    return filters
