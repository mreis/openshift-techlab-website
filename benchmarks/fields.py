import os
import json
import numpy
from benchmarks.utils import first
from django.conf import settings

elements_to_remap = ["agg"]

function_mapping = {
    "agg": {
        "first": first,
        "mean": numpy.mean,
    },
}

def init_fields(json_file, groups):
    with open(os.path.join(settings.BASE_DIR, json_file), 'r') as f:
        all_fields = json.load(f)

    fields = []
    for group in groups:
        fields += all_fields[group]

    for element in elements_to_remap:
        for field in fields:
            field[element] = function_mapping[element][field[element]]

    return fields
