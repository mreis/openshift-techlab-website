#####
# Django includes
###
from django import forms

#####
# Third-Party includes
###
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Submit
from guardian.shortcuts import get_objects_for_user

#####
# Project includes
###
from SSD.models import SSD, SSDInterface
from hardware_configuration.models import CPU, CPUArchitecture, CPUMicroArchitecture, CPUMicroArchitectureVariant, HardwareConfiguration
from manufacturer.models import Manufacturer


class SelectCPUs(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()

        self.helper.form_method = 'post'
        self.helper.form_enctype = 'multipart/form-data'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.form_action = '/cpu_benchmarks'
        self.user = kwargs.pop('user', None)

        available_hardware_configurations = get_objects_for_user(
            self.user,
            klass=HardwareConfiguration,
            perms='view_bench'
        )
        cpus_set = CPU.objects.filter(hardware_configurations__in=available_hardware_configurations).distinct()
        architectures_set = CPUArchitecture.objects.filter(cpus__in=cpus_set).distinct()
        microarchitectures_set = CPUMicroArchitecture.objects.filter(cpus__in=cpus_set).distinct()
        variants_set = CPUMicroArchitectureVariant.objects.filter(cpus__in=cpus_set).distinct()
        manufacturers_set = Manufacturer.objects.filter(cpus__in=cpus_set).distinct()

        super(SelectCPUs, self).__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Fieldset(
              'Manufacturer',
              'manufacturers'
            ),
            Fieldset(
              'CPU architecture',
              'architectures',
              'microarchitectures',
              'variants'
            ),
            Fieldset(
              'CPU specs',
              'min_f',
              'max_f'
            ),
            Fieldset(
              'Individual CPU model(s)',
              'cpus'
            )
        )

        self.fields['cpus'].queryset = cpus_set
        self.fields['architectures'].queryset = architectures_set
        self.fields['microarchitectures'].queryset = microarchitectures_set
        self.fields['variants'].queryset = variants_set
        self.fields['manufacturers'].queryset = manufacturers_set

    manufacturers = forms.ModelMultipleChoiceField(
        Manufacturer.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select CPU manufacturer(s)"
    )
    architectures = forms.ModelMultipleChoiceField(
        CPUArchitecture.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select CPU architecture(s)"
    )
    microarchitectures = forms.ModelMultipleChoiceField(
        CPUMicroArchitecture.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select CPU microarchitecture(s)"
    )
    variants = forms.ModelMultipleChoiceField(
        CPUMicroArchitectureVariant.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select CPU microarchitecture variant(s)")
    cpus = forms.ModelMultipleChoiceField(
        CPU.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select individual CPU model(s)"
    )

    min_f = forms.IntegerField(
        required=False,
        label="Filter out CPUs under a given base frequency (MHz)"
    )
    max_f = forms.IntegerField(
        required=False,
        label="Filter out CPUs above a given base frequency (MHz)"
    )
    # Add filter based on nb_sockets?

class SelectSSDs(forms.Form):
    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()

        self.helper.form_method = 'post'
        self.helper.form_enctype = 'multipart/form-data'
        self.helper.add_input(Submit('submit', 'Submit'))
        self.helper.form_action = '/ssd_benchmarks'
        self.user = kwargs.pop('user', None)

        available_ssds = get_objects_for_user(
            self.user,
            klass=SSD,
            perms='view_bench'
        )
        manufacturers_set = Manufacturer.objects.filter(ssds__in=available_ssds).distinct()
        interfaces_set = SSDInterface.objects.filter(ssds__in=available_ssds).distinct()

        super(SelectSSDs, self).__init__(*args, **kwargs)

        self.helper.layout = Layout(
            Fieldset(
                'Manufacturer',
                'manufacturers'
            ),
            Fieldset(
                'SSD specs',
                'interfaces',
                'min_capacity',
                'max_capacity'
            ),
            Fieldset(
                'Inidividual SSD model(s)',
                'ssds'
            )
        )

        self.fields['ssds'].queryset = available_ssds
        self.fields['manufacturers'].queryset = manufacturers_set
        self.fields['interfaces'].queryset = interfaces_set

    manufacturers = forms.ModelMultipleChoiceField(
        Manufacturer.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select SSD manufacturer(s)"
    )
    interfaces = forms.ModelMultipleChoiceField(
        SSDInterface.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select SSD interface(s)"
    )
    ssds = forms.ModelMultipleChoiceField(
        SSD.objects.none(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label="Select individual SSD model(s)"
    )

    min_capacity = forms.IntegerField(
        required=False,
        label="Filter out SSDs under a given capacity (GB)"
    )
        
    max_capacity = forms.IntegerField(
        required=False,
        label="Filter out SSDs over a given capacity (GB)"
    )
