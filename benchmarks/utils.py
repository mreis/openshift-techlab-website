import decimal
import itertools
from guardian.shortcuts import get_perms, get_objects_for_user
from hardware_configuration.models import HardwareConfiguration
from SSD.models import SSD
from benchmarks.models import HEPSPEC06, SSDBenchL2
from benchmarks.forms import SelectCPUs

def is_integer(x):
    try:
        f_x = float(x)
        i_x = int(x)
        return f_x == i_x
    except:
        return False


def is_float(x):
    try:
        f_x = float(x)
        i_x = int(x)
        return f_x != i_x
    except:
        return False


def is_list_of_integer(l):
    for x in l:
        if not is_integer(x):
            return False
    return True


def is_list_of_floats(l):
    for x in l:
        if not is_float(x):
            return False
    return True


def int_or_none(i):
    return int(i) if i else None


def dec_or_none(d):
    return decimal.Decimal(d) if d else None


def first(x):
    return x.iloc[0]


gen_id = itertools.count()


def checkbox_group_build_checked_list(filter_name):
    return """
    var {name}_checked = [];
    if ( {name}_obj.active.length > 0 ) {{
        for ( id of {name}_obj.active ) {{
            {name}_checked.push({name}_obj.labels[id])
        }}
    }} else {{
        for ( label of {name}_obj.labels ) {{
            {name}_checked.push(label)
        }}
    }}
    """.format(name=filter_name)


def get_runs_for_user(request, form):
    filters=None
    hw_set = get_objects_for_user(
        request.user,
        klass=HardwareConfiguration,
        perms='view_bench'
    )
    if request.method == 'POST':
      if form.is_valid():
        data = form.cleaned_data
        if data['architectures']:
          hw_set = hw_set.filter(cpu__architecture__in=data['architectures'])
        if data['manufacturers']:
          hw_set = hw_set.filter(cpu__manufacturer__in=data['manufacturers'])
        if data['microarchitectures']:
          hw_set = hw_set.filter(cpu__microarchitecture__in=data['microarchitectures'])
        if data['variants']:
          hw_set = hw_set.filter(cpu__microarchitecture_variant__in=data['variants'])
        if data['cpus']:
          hw_set = hw_set.filter(cpu__in=data['cpus'])
        if data['min_f']:
          hw_set = hw_set.filter(cpu__base_frequency__gte=data['min_f'])
        if data['max_f']:
          hw_set = hw_set.filter(cpu__base_frequency__lte=data['max_f'])

    return HEPSPEC06.objects.filter(
        system_configuration__hardware_configuration__in=hw_set
    )


def get_allowed_ssds_for_user(request):
    return get_objects_for_user(
        request.user,
        klass=SSD,
        perms='view_bench')


def get_allowed_ssd_benchmark_entries_for_user(request, modelclass):
    allowed_ssds = get_objects_for_user(
        request.user,
        klass=SSD,
        perms='view_bench')
    return modelclass.objects.filter(ssd__in=allowed_ssds)


def get_l2_entries_for_user(request, form):
    filters=None
    ssd_set = get_objects_for_user(
        request.user,
        klass=SSD,
        perms='view_bench'
    )
    if request.method == 'POST':
        if form.is_valid():
            data = form.cleaned_data
            if data['manufacturers']:
                ssd_set = ssd_set.filter(manufacturer__in=data['manufacturers'])
            if data['interfaces']:
                ssd_set = ssd_set.filter(interface__in=data['interfaces'])
            if data['min_capacity']:
                ssd_set = ssd_set.filter(capacity__gte=data['min_capacity'])
            if data['max_capacity']:
                ssd_set = ssd_set.filter(capacity__lte=data['max_capacity'])
            if data['ssds']:
                pks_set = [ssd.pk for ssd in data['ssds']]
                ssd_set = ssd_set.filter(pk__in=pks_set)

    return SSDBenchL2.objects.filter(ssd__in=ssd_set)


def init_filter_values(filters):
    js_code = ""
    for k, v in filters.items():
        if v['type'].startswith('checkbox'):
            js_code += """
var {key}_value = [];
if ( {key}.active.length > 0 ) {{
    for ( id of {key}.active ) {{
        {key}_value.push({key}.labels[id])
    }}
}} else {{
    for ( label of {key}.labels ) {{
        {key}_value.push(label)
    }}
}}
""".format(key=k)
        elif v['type'] == 'select' or v['type'].startswith('slider'):
            js_code += "var {key}_value = {key}.value;\n".format(key=k)
        elif v['type'].startswith('date_picker'):
            js_code += """
var {key}_w_tz_offset = new Date({key}.value);
var {key}_tz_offset = {key}_w_tz_offset.getUTCHours() == 0 ? 0 : {key}_w_tz_offset.getTimezoneOffset() * 60000;
var {key}_value = new Date({key}_w_tz_offset - {key}_tz_offset);
""".format(key=k)

    return js_code


def init_filter_condition(filters, suffix="", source_suffix=""):
    conditions = []
    suffix_ = "_' + {}".format(suffix) if suffix else "'"
    source_suffix_ = "_{}".format(source_suffix) if source_suffix else ""
    for k, v in filters.items():
        if v['type'].startswith('checkbox'):
            conditions.append("{key}_value.indexOf(original_data{source_suffix}['{ds_column}{suffix}][i]) >= 0".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
        elif v['type'] == 'select':
            conditions.append("{key}_value === original_data{source_suffix}['{ds_column}{suffix}][i]".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
        elif v['type'] == 'slider_min':
            conditions.append("original_data{source_suffix}['{ds_column}{suffix}][i] >= parseFloat({key}_value)".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
        elif v['type'] == 'slider_max':
            conditions.append("original_data{source_suffix}['{ds_column}{suffix}][i] <= parseFloat({key}_value)".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
        elif v['type'] == 'date_picker_min':
            conditions.append("original_data{source_suffix}['{ds_column}{suffix}][i] >= {key}_value".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
        elif v['type'] == 'date_picker_max':
            conditions.append("original_data{source_suffix}['{ds_column}{suffix}][i] <= {key}_value".format(key=k, ds_column=v['ds_column'], suffix=suffix_, source_suffix=source_suffix_))
    for c in conditions:
        print(c)
    return " && ".join(conditions)


def emit_source_change(sources):
    js_code = ""
    for ds in sources:
        js_code += "source_{}.change.emit();".format(ds['pk'][0])

    return js_code


#def init_datasources(sources, original_sources):
#    js_code = ""
#    for ds in sources:
#        js_code += "var data_{} = source_{}.data;".format(ds['pk'][0])
#    for ds in original_sources:
#        js_code += "var original_data_{} = original_source_{}.data;".format(ds['pk'][0])
#
#    return js_code


def init_datasources(sources, original_sources):
    js_code = "var datasources = {};"
    for ds in sources:
        js_code += "datasources['{}'] = "
