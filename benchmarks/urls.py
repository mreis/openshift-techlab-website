from django.conf.urls import url

from . import views

urlpatterns = [
#    url(r'^$', views.home, name='home'),
#    url(r'^home_dev$', views.home_dev, name='home_dev'),
#    url(r'^cpu$', views.cpu_benchmarks, name='cpu_benchmarks'),
#    url(r'^cpu_update_bokeh$', views.cpu_benchmarks_update_bokeh, name='cpu_benchmarks_update_bokeh'),
#    url(r'^ssd$', views.ssd_benchmarks, name='ssd_benchmarks'),
#    url(r'^ssd_dev$', views.ssd_benchmarks_dev, name='ssd_benchmarks_dev'),
#    url(r'^ssd_dev2$', views.ssd_benchmarks_dev2, name='ssd_benchmarks_dev2'),
#    url(r'^import_benchmarks/$', views.import_benchmarks, name='import_benchmarks'),
#    url(r'^import_hepspec_benchmarks/$', views.import_hepspec_benchmarks, name='import_hepspec_benchmarks'),
#    url(r'^bokeh_debug_01$', views.bokeh_debug_01, name='bokeh_debug_01'),
#    url(r'^cpu_update_bokeh_multiple_ds$', views.cpu_update_bokeh_multiple_ds, name='cpu_update_bokeh_multiple_ds'),
#    url(r'^chartit_01$', views.chartit_01, name='chartit_01'),
]
