# -*- coding: utf-8 -*-
import os
import json
from django.conf import settings

def init_plots(json_file, groups):
    with open(os.path.join(settings.BASE_DIR, json_file), 'r') as f:
        all_plots = json.load(f)

    plots = []
    for group in groups:
        plots += all_plots[group]

    return plots
