from __future__ import unicode_literals

from django.apps import AppConfig


class SoftwareConfigurationConfig(AppConfig):
    name = 'software_configuration'
    verbose_name = 'Software Configuration'
