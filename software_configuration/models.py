# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django.db import models


@python_2_unicode_compatible
class OS(models.Model):

    class Meta:
        verbose_name = _('Operating System')
        verbose_name_plural = _('Operating Systems')

    def __str__(self):
        return "{} - {}".format(self.name, self.version)

    name = models.CharField(max_length=255)
    version = models.CharField(max_length=255)
    release_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        verbose_name='release_name'
    )


@python_2_unicode_compatible
class LinuxKernel(models.Model):

    class Meta:
        verbose_name = _('Linux kernel')
        verbose_name_plural = _('Linux kernels')

    def __str__(self):
        return self.version

    version = models.CharField(max_length=255)


@python_2_unicode_compatible
class SoftwareConfiguration(models.Model):

    class Meta:
        verbose_name = _('Software configuration')
        verbose_name_plural = _('Software configurations')

    def __str__(self):
        return "{name} — {os} {os_version}".format(
            name=self.name,
            os=self.os.name,
            os_version=self.os.version,
        )

    name = models.CharField(max_length=255)
    os = models.ForeignKey('OS', on_delete=models.CASCADE)
