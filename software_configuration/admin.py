from django.contrib import admin

from bulk_admin import BulkModelAdmin

from .models import OS, LinuxKernel, SoftwareConfiguration


@admin.register(OS)
class OSAdmin(BulkModelAdmin):
    pass


@admin.register(LinuxKernel)
class LinuxKernelAdmin(BulkModelAdmin):
    pass


@admin.register(SoftwareConfiguration)
class SoftwareConfigurationAdmin(BulkModelAdmin):
    pass
