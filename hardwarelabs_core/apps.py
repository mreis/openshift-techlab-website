from __future__ import unicode_literals

from django.apps import AppConfig


class HardwarelabsCoreConfig(AppConfig):
    name = 'hardwarelabs_core'
