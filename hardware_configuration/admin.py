from django.contrib import admin

from bulk_admin import BulkModelAdmin
from guardian.admin import GuardedModelAdmin

from hardwarelabs_core.admin import GuardedModelAdminMod

from .models import CPU, CPUArchitecture, CPUMicroArchitecture, CPUMicroArchitectureVariant, HardwareConfiguration, Socket


class SocketInline(admin.TabularInline):
    model = Socket
    extras = 1


@admin.register(CPU)
class CPUAdmin(BulkModelAdmin):
    pass


@admin.register(CPUArchitecture)
class ArchitectureAdmin(BulkModelAdmin):
    pass


@admin.register(CPUMicroArchitecture)
class MicroArchitectureAdmin(BulkModelAdmin):
    pass


@admin.register(CPUMicroArchitectureVariant)
class MicroArchitectureVariantAdmin(BulkModelAdmin):
    pass


@admin.register(HardwareConfiguration)
class HardwareConfigurationAdmin(GuardedModelAdminMod, BulkModelAdmin):
   inlines = (SocketInline,)
