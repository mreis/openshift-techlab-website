from __future__ import unicode_literals

from django.apps import AppConfig


class HardwareConfigurationConfig(AppConfig):
    name = 'hardware_configuration'
    verbose_name = 'Hardware Configuration'
