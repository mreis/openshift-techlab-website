# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _

from django.db import models

import datetime

from manufacturer.models import Manufacturer

YEAR_CHOICES = [(y,y) for y in range(1980, datetime.date.today().year+1)]
MONTH_CHOICES = [(m,m) for m in range(1, 13)]
DAY_CHOICES = [(d,d) for d in range(1, 32)]

def validate_date(y,m,d):
  try:
    datetime.date(y,m,d)
  except ValueError:
    return False
  return True


@python_2_unicode_compatible
class CPU(models.Model):

    class Meta:
        verbose_name = _('CPU')
        verbose_name_plural = _('CPUs')

    def __str__(self):
        return "{} {} {}GHz".format(
            self.manufacturer.name,
            self.model_name,
            self.base_frequency
        )

    manufacturer = models.ForeignKey(
        Manufacturer,
        on_delete=models.PROTECT,
        related_name='cpus',
        limit_choices_to={'cpu': True},
    )
    model_name = models.CharField(max_length=255, verbose_name='model name')
    lscpu_model_name = models.CharField(
        max_length=255,
        verbose_name='Model name, as listed by lscpu',
        unique=True,
        null=True, blank=True
    )
    nb_cores = models.IntegerField(
        verbose_name="number of cores",
        null=True, blank=True
    )
    nb_threads = models.IntegerField(
        verbose_name="number of virtual cores",
        null=True, blank=True
    )
    architecture = models.ForeignKey(
        'CPUArchitecture',
        related_name='cpus',
        on_delete=models.PROTECT,
    )
    microarchitecture = models.ForeignKey(
        'CPUMicroArchitecture',
        related_name='cpus',
        on_delete=models.PROTECT,
        null=True, blank=True
    )
    microarchitecture_variant = models.ForeignKey(
        'CPUMicroArchitectureVariant',
        related_name='cpus',
        on_delete=models.PROTECT,
        null=True, blank=True
    )
    base_frequency = models.IntegerField(
        verbose_name="base frequency (MHz)",
#        max_digits=5,
#        decimal_places=2,
        null=True, blank=True,
        default=0
    )
    max_frequency = models.IntegerField(
        verbose_name="max frequency (MHz)",
#        max_digits=5,
#        decimal_places=2,
        null=True, blank=True,
        default=0
    )
    release_year = models.SmallIntegerField(
        choices=YEAR_CHOICES,
        null=True, blank=True
    )
    release_month = models.SmallIntegerField(
        choices=MONTH_CHOICES,
        null=True, blank=True
    )
    release_day = models.SmallIntegerField(
        choices=DAY_CHOICES,
        null=True, blank=True,
        validators=[validate_date]
    )


@python_2_unicode_compatible
class CPUArchitecture(models.Model):

    class Meta:
        verbose_name = _('CPU architecture')
        verbose_name_plural = _('CPU architectures')
        ordering = ['name']

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255)
#    shortname = models.CharField(max_length=255)


@python_2_unicode_compatible
class CPUMicroArchitecture(models.Model):

    class Meta:
        verbose_name = _('CPU microarchitecture')
        verbose_name_plural = _('CPU microarchitectures')
        ordering = ['name']

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255)
    architecture = models.ForeignKey(
        'CPUArchitecture',
        related_name='microarchitectures',
        on_delete=models.CASCADE,
        null=True, blank=True
    )


@python_2_unicode_compatible
class CPUMicroArchitectureVariant(models.Model):

    class Meta:
        verbose_name = _('CPU microarchitecture variant (codename)')
        verbose_name_plural = _('CPU microarchitecture variants (codenames)')
        ordering = ['name']

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255)
    process = models.IntegerField(help_text="Fabrication process size (in nm)")
    microarchitecture = models.ForeignKey(
        'CPUMicroArchitecture',
        related_name='variants',
        on_delete=models.CASCADE,
        null=True, blank=True
    )


@python_2_unicode_compatible
class HardwareConfiguration(models.Model):

    class Meta:
        verbose_name = _('Hardware configuration')
        verbose_name_plural = _('Hardware configurations')
        permissions = {
            ('view_bench', 'View benchmarks for this hardware'),
        }

    def __str__(self):
        return self.name

    name = models.CharField(max_length=255)
    cpu = models.ManyToManyField(
        'CPU',
        through='Socket',
        related_name='hardware_configurations'
    )
    ram = models.IntegerField(
        help_text="System RAM in GB",
        null=True, blank=True
    )
    storage = models.IntegerField(
        help_text="System storage size in GB",
        null=True, blank=True
    )
    comment = models.CharField(
        max_length=255,
        null=True, blank=True
    )

#    visible = models.BooleanField(default=False)
#    owner = models.CharField(max_length=255)
#    vendor = models.CharField(max_length=255)

class Socket(models.Model):

    class Meta:
        verbose_name = _('Socket')
        verbose_name_plural = _('Sockets')

    cpu = models.ForeignKey(CPU)
    hardware_configuration = models.ForeignKey(HardwareConfiguration)
