# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-11-29 10:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('hardware_configuration', '0002_auto_20181129_0915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cpu',
            name='manufacturer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.PROTECT, related_name='cpus', to='manufacturer.Manufacturer'),
        ),
    ]
