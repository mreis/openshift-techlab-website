from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from welcome.views import health
from benchmarks.views import benchmarks_list, cpu_benchmarks, ssd_benchmarks, select_hepspec_source, select_ssd_benchmark_source
from home.views import home
from entries_import.views import import_benchmarks, import_hepspec_benchmarks

from django.contrib.auth.views import login, logout

urlpatterns = [
    # Home
    url(r'^$', home, name='home'),
    # Benchmarks
    url(r'^benchmarks_list$', benchmarks_list, name='benchmarks_list'),
    url(r'^cpu_benchmarks$', cpu_benchmarks, name='cpu_benchmarks'),
    url(r'^select_cpus$', select_hepspec_source, name='cpu_benchmarks_select'),
    url(r'^select_ssds$', select_ssd_benchmark_source, name='ssd_benchmarks_select'),
    url(r'^ssd_benchmarks$', ssd_benchmarks, name='ssd_benchmarks'),
    # Import
    url(r'^ssd_import$', import_benchmarks, name='import_benchmarks'),
    url(r'^hepspec_import$', import_hepspec_benchmarks, name='import_hepspec_benchmarks'),
    # Admin pages
    url(r'^admin/', include(admin.site.urls)),
    # SSO-related pages
    url(r'^Shibboleth.sso/$', login, name='login'),
    url(r'^Shibboleth.sso/Logout$', logout, name='logout'),
    # Health check
    url(r'^health$', health),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
